<?php

/**
 * @file
 * Contains the base class for collections of joind.in objects
 *
 * Many calls to the API return collections of objects, eg all events on the
 * site, all talks at an event, all comments on a talk/event
 */

/**
 * Iterable base class for Collection objects.
 */
class JoindinCollection implements ArrayAccess, Iterator, Countable {
  // Array of the iterable values.
  protected $collection = array();
  protected $keys = array();
  protected $currentKey;

  /**
   * Whether an offset exists.
   * @link http://php.net/manual/en/arrayaccess.offsetexists.php
   *
   * @param mixed $offset
   *   An offset to check for.
   *
   * @return bool
   *   true on success or false on failure.
   *
   * The return value will be casted to boolean if non-boolean was returned.
   */
  public function offsetExists($offset) {
    return array_key_exists($this->collection, $offset);
  }

  /**
   * Offset to retrieve.
   * @link http://php.net/manual/en/arrayaccess.offsetget.php
   *
   * @param mixed $offset
   *   The offset to retrieve.
   *
   * @return mixed
   *   Can return all value types.
   */
  public function offsetGet($offset) {
    return $this->collection[$offset];
  }

  /**
   * Offset to set.
   * @link http://php.net/manual/en/arrayaccess.offsetset.php
   *
   * @param mixed $offset
   *   The offset to assign the value to.
   * @param mixed $value
   *   The value to set.
   */
  public function offsetSet($offset, $value) {
    $this->collection[$offset] = $value;
  }
  /**
   * Offset to unset.
   * @link http://php.net/manual/en/arrayaccess.offsetunset.php
   *
   * @param mixed $offset
   *   The offset to unset.
   */
  public function offsetUnset($offset) {
    unset($this->collection[$offset]);
  }
  /**
   * Count elements of an object.
   * @link http://php.net/manual/en/countable.count.php
   *
   * @return int
   *   The custom count as an integer.
   *
   * The return value is cast to an integer.
   */
  public function count() {
    return count($this->collection);
  }

  /**
   * Return the current element.
   * @link http://php.net/manual/en/iterator.current.php
   *
   * @return mixed
   *   Can return any type.
   */
  public function current() {
    return $this->collection[$this->currentKey];
  }

  /**
   * Return the key of the current element.
   * @link http://php.net/manual/en/iterator.key.php
   *
   * @return mixed
   *   scalar on success, or null on failure.
   */
  public function key() {
    return $this->currentKey;
  }

  /**
   * Move forward to next element.
   * @link http://php.net/manual/en/iterator.next.php
   */
  public function next() {
    $keys = array_keys($this->collection);
    $current_position = array_search($this->currentKey, $keys);

    // Only increment the position if the end of the array is not yet reached.
    if ($current_position < count($this->collection) - 1) {
      $next_position = $current_position + 1;
      $next_key = $keys[$next_position];
      $this->currentKey = $next_key;
    }
    else {
      $this->currentKey = NULL;
    }
  }

  /**
   * Rewind the Iterator to the first element.
   * @link http://php.net/manual/en/iterator.rewind.php
   */
  public function rewind() {
    $keys = array_keys($this->collection);
    $this->currentKey = reset($keys);
  }

  /**
   * Checks if current position is valid.
   * @link http://php.net/manual/en/iterator.valid.php
   *
   * @return bool
   *   The return value will be casted to boolean and then evaluated.
   *   Returns true on success or false on failure.
   */
  public function valid() {
    if (!is_int($this->currentKey) && !is_string($this->currentKey)) {
      return FALSE;
    }
    return array_key_exists($this->currentKey, $this->collection);
  }
}
