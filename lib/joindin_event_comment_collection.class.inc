<?php
/**
 * @file
 * Definition of the JoindinEventCommentCollection class.
 */

/**
 * Class JoindinEventCommentCollection
 */
class JoindinEventCommentCollection extends JoindinCollection {

  public $commentList = array();

  /**
   * Constructor.
   *
   * @param string $data
   *   JSON data from the JoindIn API representing a list of 1 or more comments.
   */
  public function __construct($data) {
    $this->constructFromJSON($data);
  }

  /**
   * Build an event using a JSON data structure.
   *
   * @param string $json
   *   JSON data from the JoindIn API representing a list of 1 or more events.
   */
  public function constructFromJSON($json) {
    // Decode the JSON.
    $comments = drupal_json_decode($json);

    // The API wraps the response for a single talk with 2 tiers of array:
    // $events = array('comments' => array(0 => array(...), 1 => array(...))).
    if (is_array($comments) && isset($comments['comments'])) {
      foreach ($comments['comments'] as $comment_data) {
        // If an array is passed to the JoindinEvent constructor, the event
        // data is automatically populated from the array properties.
        $comment = new JoindinEventComment($comment_data);
        // Index by comment ID.
        $this->collection[$comment->comment_id] = $comment;
      }
    }
  }

  /**
   * Get the comments in this collection.
   *
   * @return array
   *   An unordered array of JoindinEventComment objects.
   */
  public function getComments() {
    return $this->commentList;
  }

  /**
   * Get a comment in this collection.
   *
   * @param int $comment_id
   *   The comment_id of the desired talk. If omitted, the first resource in the
   *   list will be returned.
   *
   * @return JoindinEventComment
   *   A single comment object
   */
  public function getComment($comment_id = NULL) {
    if (is_null($comment_id)) {
      return reset($this->commentList);
    }
    elseif (array_key_exists($this->commentList, $comment_id)) {
      return $this->commentList[$comment_id];
    }
    else {
      return NULL;
    }
  }
}
