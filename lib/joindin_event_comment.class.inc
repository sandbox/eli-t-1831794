<?php

/**
 * @file
 * Joindin Event Comment class
 *
 * Note event and talk comments are different
 */

class JoindinEventComment extends JoindinRetriever {
  /**
   * Constructor.
   *
   * The argument can be either a JSON array (representing the response from
   * the JoindIn API), or an array (representing the decoded JSON).
   */
  public function __construct($data) {
    switch (gettype($data)) {
      case 'string':
        $this->constructFromJSON($data);
        break;

      case 'array':
        $this->constructFromArray($data);
        break;

      default:
        // Throw an exception.
    }
    $this->inferProperties();
  }

  /**
   * Build an event comment using a JSON data structure.
   *
   * @param string $json
   *   JSON data from the JoindIn API representing a list of 1 or more comments.
   */
  public function constructFromJSON($json) {
    // Decode the JSON.
    $comments = drupal_json_decode($json);

    // The API wraps the response for a single talk with 2 tiers of array.
    if (is_array($comments) && isset($comments['comments'])) {
      // Only take the first comment, even if there are multiple comments.
      $comment = reset($comment['comments']);
    }

    $this->constructFromArray($comments);
  }

  /**
   * Build a comment using a decoded JSON array structure.
   *
   * @param string $comment
   *   Decoded JSON data from the JoindIn API representing a single comment.
   */
  protected function constructFromArray($comment) {
    foreach (self::validProperties() as $property => $is_required) {
      // Validate required fields.
      if ($is_required && !array_key_exists($property, $comment)) {
        // Throw exception.
      }
      $this->$property = $comment[$property];
    }
  }

  /**
   * List property keys.
   *
   * List the property keys that a comment may have, and whether the property is
   * required.
   *
   * @return array
   *   An array indexed by the property-name, where the value is a boolean:
   *   TRUE if the property is required.
   */
  protected static function validProperties() {
    return array(
      'comment' => TRUE,
      'created_date' => FALSE,
      'user_display_name' => FALSE,
      'user_uri' => FALSE,
      'comment_uri' => TRUE,
      'verbose_comment_uri' => FALSE,
      'event_uri' => TRUE,
      'event_comments_uri' => TRUE,
    );
  }

  /**
   * Infer undeclared properties from the properties provided.
   */
  protected function inferProperties() {
    if (preg_match('#event_comments/([0-9]+)$#', $this->comment_uri, $matches)) {
      $this->comment_id = $matches[1];
    }
  }
}
